/* Problem URL: https://uva.onlinejudge.org/external/114/11498.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

enum quadrant
{
  NORTH_EAST,
  NORTH_WEST,
  SOUTH_WEST,
  SOUTH_EAST,
  BORDER,
};

enum quadrant find_quadrant (const int split_x, const int split_y,
			     const int x, const int y);

int
main (void)
{
  unsigned nqueries;
  while (scanf ("%u", &nqueries) == 1)
    {
      if (nqueries == 0)
	break;

      int split_x, split_y;
      if (scanf ("%d %d", &split_x, &split_y) != 2)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      for (unsigned i = 0; i < nqueries; ++i)
	{
	  int x, y;
	  if (scanf ("%d %d", &x, &y) != 2)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }

	  enum quadrant quad = find_quadrant (split_x, split_y, x, y);
	  const char *loc = NULL;
	  switch (quad)
	    {
	    case NORTH_WEST:
	      loc = "NO";
	      break;
	    case NORTH_EAST:
	      loc = "NE";
	      break;
	    case SOUTH_EAST:
	      loc = "SE";
	      break;
	    case SOUTH_WEST:
	      loc = "SO";
	      break;
	    case BORDER:
	      loc = "divisa";
	      break;
	    default:
	      fprintf (stderr, __LOC_PRE__ "Error: unexpected quadrant: %d\n",
		       quad);
	      exit (EXIT_FAILURE);
	    }

	  if (puts (loc) == EOF)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }
	}
    }

  exit (EXIT_SUCCESS);
}

enum quadrant
find_quadrant (const int split_x, const int split_y, const int x, const int y)
{
  const int x_shifted = x - split_x;
  const int y_shifted = y - split_y;
  if (x_shifted == 0 || y_shifted == 0)
    return BORDER;

  if (x_shifted > 0)
    {
      if (y_shifted > 0)
	return NORTH_EAST;
      else
	return SOUTH_EAST;
    }

  else
    {
      if (y_shifted > 0)
	return NORTH_WEST;
      else
	return SOUTH_WEST;
    }
}
