/* Problem URL: https://uva.onlinejudge.org/external/115/11547.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

int
main (void)
{
  unsigned ntests;
  if (scanf ("%u", &ntests) != 1)
    {
      perror (__LOC__);
      exit (EXIT_FAILURE);
    }

  for (unsigned i = 0; i < ntests; ++i)
    {
      double n;
      if (scanf ("%lf", &n) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}


      unsigned ans =
	((int) (floor (fabs ((((n * 567 / 9) + 7492) * 235 / 47) - 498))) %
	 100) / 10;
      if (printf ("%u\n", ans) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}
    }

  exit (EXIT_SUCCESS);
}
