/* Problem URL: https://uva.onlinejudge.org/external/113/11340.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

struct payment
{
  char letter;
  unsigned ncents;
};
typedef struct payment payment_t;

int payment_char_cmp (const void *const p1, const void *const p2);
int payment_char_search (const void *const k, const void *const p);

int
main (void)
{
  unsigned ntests;

  if (scanf ("%u", &ntests) != 1)
    {
      perror (__LOC__);
      exit (EXIT_FAILURE);
    }

  for (unsigned i = 0; i < ntests; ++i)
    {
      unsigned npayments;
      if (scanf ("%u\n", &npayments) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      payment_t *payments;
      if ((payments =
	   (payment_t *) malloc (npayments * sizeof *payments)) == NULL)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      for (unsigned j = 0; j < npayments; ++j)
	if (scanf ("%c %u\n", &payments[j].letter, &payments[j].ncents) != 2)
	  {
	    perror (__LOC__);
	    exit (EXIT_FAILURE);
	  }

      qsort (payments, npayments, sizeof *payments, payment_char_cmp);

      unsigned nlines;
      if (scanf ("%u\n", &nlines) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      unsigned total = 0;
      unsigned count = 0;
      int c;
      while (count < nlines && (c = getchar ()) != EOF)
	{
	  if (c == '\n')
	    ++count;

	  char letter = c;
	  payment_t *payment =
	    (payment_t *) bsearch (&letter, payments, npayments,
				   sizeof *payments, payment_char_search);
	  if (payment)
	    total += payment->ncents;
	}

      if (c == EOF && count + 1 < nlines)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      if (printf ("%u.%02u$\n", total / 100, total % 100) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      free (payments);
    }

  exit (EXIT_SUCCESS);
}

int
payment_char_cmp (const void *const p1, const void *const p2)
{
  const char c1 = ((payment_t *) p1)->letter;
  const char c2 = ((payment_t *) p2)->letter;

  return ((c1 > c2) ? 1 : (c1 == c2) ? 0 : -1);
}

int
payment_char_search (const void *const k, const void *const p)
{
  const char c1 = *((char *) k);
  const char c2 = ((payment_t *) p)->letter;

  return ((c1 > c2) ? 1 : (c1 == c2) ? 0 : -1);
}
