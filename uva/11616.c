/* Problem URL: https://uva.onlinejudge.org/external/116/11616.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

#define MAXLINELEN 256		/* safe guess for longest length numeral */

int rdigitval (const char rdigit);

int
main (void)
{
  char buf[MAXLINELEN];

  while (scanf ("%s", buf) == 1)
    if (isdigit (buf[0]))
      {
	unsigned num;
	if (sscanf (buf, "%u", &num) != 1)
	  {
	    perror (__LOC__);
	    exit (EXIT_FAILURE);
	  }

	const static unsigned digit_vals[] =
	    { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
	static const char *const digits[] =
	    { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V",
	  "IV", "I"
	};



	while (num > 0)
	  {
	    size_t i = 0;
	    while (i < LENGTH (digit_vals) && digit_vals[i] > num)
	      ++i;
	    if (printf ("%s", digits[i]) < 0)
	      {
		perror (__LOC__);
		exit (EXIT_FAILURE);
	      }
	    num -= digit_vals[i];
	  }
	if (putchar ('\n') == EOF)
	  {
	    perror (__LOC__);
	    exit (EXIT_FAILURE);
	  }
      }
    else
      {
	int total = 0;
	const size_t blen = strlen (buf);
	for (size_t i = 0; i < blen; ++i)
	  total +=
	      rdigitval (buf[i]) *
	      ((rdigitval (buf[i]) < rdigitval (buf[i + 1])) ? -1 : 1);

	if (printf ("%d\n", total) < 0)
	  {
	    perror (__LOC__);
	    exit (EXIT_FAILURE);
	  }
      }


  exit (EXIT_SUCCESS);
}


int
rdigitval (const char rdigit)
{
  switch (rdigit)
    {
    case 'I':
      return 1;
    case 'V':
      return 5;
    case 'X':
      return 10;
    case 'L':
      return 50;
    case 'C':
      return 100;
    case 'D':
      return 500;
    case 'M':
      return 1000;
    default:
      return 0;
    }
}
