/* Problem URL: https://icpcarchive.ecs.baylor.edu/external/30/3012.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

int gcd (const int a, const int b);
int print_rational (const int sign, const int w, const int num,
		    const int dem);

int
main (void)
{
  unsigned ncase = 0;
  for (;;)
    {
      unsigned n;
      if (scanf ("%u", &n) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      if (n == 0)
	break;

      ++ncase;
      if (printf ("Case %u:\n", ncase) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      int cur;
      int total = 0;
      for (unsigned i = 0; i < n; ++i)
	{
	  if (scanf ("%d", &cur) != 1)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }
	  total += cur;
	}

      const int sign = (total < 0) ? -1 : 1;
      const int w = abs (total) / (int) n;
      const int num = (abs (total) % n) / gcd (abs (total) % n, n);
      const int dem = n / gcd (abs (total) % n, n);

      if (num == 0)
	{
	  if (printf ("%s%d\n", (sign < 0) ? "- " : "", abs (w)) < 0)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }
	}
      else
	{
	  if (print_rational (sign, w, num, dem) < 0)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }
	}
    }


  exit (EXIT_SUCCESS);
}

int
gcd (int a, int b)
{
  int t;
  while (b != 0)
    {
      t = b;
      b = a % b;
      a = t;
    }

  return a;
}

int
print_rational (const int sign, const int w, const int num, const int dem)
{
  const unsigned sdigits = (sign < 0) ? 2 : 0;
  const unsigned nwdigits = (w == 0) ? 0 : snprintf (NULL, 0, "%d", abs (w));
  const unsigned nnum = snprintf (NULL, 0, "%d", num);
  const unsigned ndem = snprintf (NULL, 0, "%d", dem);
  const unsigned nfdigits = (nnum >= ndem) ? nnum : ndem;
  int ret;

  if ((ret = printf ("%*d\n", sdigits + nwdigits + nfdigits, num)) < 0)
    return ret;

  if ((ret = printf ("%s", (sign < 0) ? "- " : "")) < 0)
    return ret;

  if (w != 0)
    if ((ret = printf ("%d", abs (w))) < 0)
      return ret;

  for (unsigned i = 0; i < nfdigits; ++i)
    if ((ret = putchar ('-')) == EOF)
      return ret;

  if ((ret = putchar ('\n')) == EOF)
    return ret;

  if ((ret = printf ("%*d\n", sdigits + nwdigits + nfdigits, dem)) < 0)
    return ret;

  return 0;
}
