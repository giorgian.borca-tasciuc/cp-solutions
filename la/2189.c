/* Problem URL: https://icpcarchive.ecs.baylor.edu/external/21/2189.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

#define BUFSIZE 32		/* safe guess for longest string */

int
main (void)
{

  unsigned ncase = 0;
  for (;;)
    {
      unsigned n;
      if (scanf ("%u", &n) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      if (n == 0)
	break;

      ++ncase;
      if (printf ("Case %u:\n", ncase) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      unsigned long long cur = 0;
      unsigned long long prev = 0;
      unsigned long long beg = 0;	/* beginning of number series */
      if (scanf ("%llu", &cur) != 1)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      prev = beg = cur;
      for (unsigned i = 1; i < n; ++i)
	{
	  if (scanf ("%llu", &cur) != 1)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }

	  if (cur == prev + 1)
	    prev = cur;
	  else if (beg != prev && beg != 0)
	    {
	      char sbegin[BUFSIZE];
	      char send[BUFSIZE];

	      if (printf ("0%llu-", beg) < 0
		  || sprintf (sbegin, "%llu\n", beg) < 0
		  || sprintf (send, "%llu\n", prev) < 0)
		{
		  perror (__LOC__);
		  exit (EXIT_FAILURE);
		}

	      bool print = false;
	      for (size_t j = 0; sbegin[j]; ++j)
		{
		  print |= sbegin[j] != send[j];
		  if (print && putchar (send[j]) == EOF)
		    {
		      perror (__LOC__);
		      exit (EXIT_FAILURE);
		    }
		}

	      beg = prev = cur;
	    }
	  else
	    {
	      if (printf ("0%llu\n", prev) < 0)
		{
		  perror (__LOC__);
		  exit (EXIT_FAILURE);
		}
	      beg = prev = cur;
	    }
	}

      if (beg != prev)
	{
	  char sbegin[BUFSIZE];
	  char send[BUFSIZE];

	  if (printf ("0%llu-", beg) < 0
	      || sprintf (sbegin, "%llu\n\n", beg) < 0
	      || sprintf (send, "%llu\n\n", prev) < 0)
	    {
	      perror (__LOC__);
	      exit (EXIT_FAILURE);
	    }

	  bool print = false;
	  for (size_t j = 0; sbegin[j]; ++j)
	    {
	      print |= sbegin[j] != send[j];
	      if (print && putchar (send[j]) == EOF)
		{
		  perror (__LOC__);
		  exit (EXIT_FAILURE);
		}
	    }
	}
      else if (printf ("0%llu\n\n", prev) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}
    }


  exit (EXIT_SUCCESS);
}
