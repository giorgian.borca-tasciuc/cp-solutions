/* Problem URL: https://icpcarchive.ecs.baylor.edu/external/42/4202 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

typedef struct
{
  unsigned short h;
  unsigned short m;
} ptime_t;

static inline int ptcmp (const ptime_t a, const ptime_t b);

int
main (void)
{
  unsigned ncases;

  if (scanf ("%u", &ncases) != 1)
    {
      perror (__LOC__);
      exit (EXIT_FAILURE);
    }


  for (unsigned i = 0; i < ncases; ++i)
    {
      ptime_t ws, we, ms, me;

      if (scanf
	  ("%hu:%hu %hu:%hu %hu:%hu %hu:%hu", &ws.h, &ws.m, &we.h, &we.m,
	   &ms.h, &ms.m, &me.h, &me.m) != 8)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      const bool miss = (ptcmp (ms, ws) >= 0 && ptcmp (ms, we) <= 0
			 && ptcmp (ws, we) < 0) || (ptcmp (me, ws) >= 0
						    && ptcmp (me, we) <= 0
						    && ptcmp (ws, we) < 0)
	|| (ptcmp (ms, ws) <= 0 && ptcmp (ms, we) <= 0 && ptcmp (ws, we) > 0)
	|| (ptcmp (ms, ws) >= 0 && ptcmp (ms, we) >= 0 && ptcmp (ws, we) > 0)
	|| (ptcmp (me, ws) <= 0 && ptcmp (me, we) <= 0 && ptcmp (ws, we) > 0)
	|| (ptcmp (me, ws) >= 0 && ptcmp (me, we) >= 0 && ptcmp (ws, we) > 0)
	|| (ptcmp (ws, ms) >= 0 && ptcmp (ws, me) <= 0 && ptcmp (ms, me) < 0)
	|| (ptcmp (we, ms) >= 0 && ptcmp (we, me) <= 0 && ptcmp (ms, me) < 0)
	|| (ptcmp (ws, ms) <= 0 && ptcmp (ws, me) <= 0 && ptcmp (ms, me) > 0)
	|| (ptcmp (ws, ms) >= 0 && ptcmp (ws, me) >= 0 && ptcmp (ms, me) > 0)
	|| (ptcmp (we, ms) <= 0 && ptcmp (we, me) <= 0 && ptcmp (ms, me) > 0)
	|| (ptcmp (we, ms) >= 0 && ptcmp (we, me) >= 0 && ptcmp (ms, me) > 0);

      if (printf ("Case %u: %s Meeting\n", i + 1, miss ? "Mrs" : "Hits") < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}
    }

  exit (EXIT_SUCCESS);
}

static inline int
ptcmp (const ptime_t a, const ptime_t b)
{
  return (a.h <
	  b.h) ? -1 : ((a.h ==
			b.h) ? ((a.m < b.m) ? -1 : (a.m == b.m) ? 0 : 1) : 1);
}
