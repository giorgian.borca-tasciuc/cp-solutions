/* Problem URL: https://icpcarchive.ecs.baylor.edu/external/31/3173.pdf */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#define STR(X) #X
#define XSTR(X) STR(X)
#define __LOC__ __FILE__ ":" XSTR (__LINE__)
#define __LOC_PRE__ __LOC__ ": "

#define LENGTH(arr) (sizeof (arr) / sizeof (arr[0]))

#define PSIZE 20

bool next_permutation (char s[], const size_t len);
bool prev_permutation (char s[], const size_t len);
short minabsdist (const char s[], const size_t len);
int ccmp (const void *const a, const void *const b);
int rccmp (const void *const a, const void *const b);

int
main (void)
{
  for (;;)
    {
      char password[PSIZE + 1];
      if (scanf ("%" XSTR (PSIZE) "s", password) != 1)
	{
	  if (feof (stdin))
	    break;

	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}

      const size_t len = strlen (password);
      char permute[PSIZE + 1];
      char best[PSIZE + 1];
      short d = minabsdist (password, len);

      strcpy (best, password);
      strcpy (permute, password);

      for (size_t i = 0; i < 10 && prev_permutation (permute, len); ++i)
	{
	  const short nd = minabsdist (permute, len);
	  if (nd >= d)
	    {
	      d = nd;
	      strcpy (best, permute);
	    }
	}

      strcpy (permute, password);
      for (size_t i = 0; i < 10 && next_permutation (permute, len); ++i)
	{
	  const short nd = minabsdist (permute, len);
	  if (nd > d)
	    {
	      d = nd;
	      strcpy (best, permute);
	    }
	}

      if (printf ("%s%hd\n", best, d) < 0)
	{
	  perror (__LOC__);
	  exit (EXIT_FAILURE);
	}
    }

  exit (EXIT_SUCCESS);
}

bool
next_permutation (char s[], const size_t len)
{
  /* find longest subsequence from end that it reverse sorted */
  size_t sbegin = len - 1;
  for (size_t i = 0; i < len - 1 && s[len - i - 2] >= s[len - i - 1]; ++i)
    sbegin = len - i - 2;

  /* whole word is reverse sorted, we can just sort and return */
  if (sbegin == 0)
    {
      qsort (s, len, sizeof (s[0]), ccmp);
      return false;
    }

  /* find smallest letter greater than s[sbegin - 1] to swap with */
  size_t sswap;
  bool f = false;
  for (size_t i = sbegin; i < len; ++i)
    if (s[i] > s[sbegin - 1]
	&& (!f || s[i] - s[sbegin - 1] < s[sswap] - s[sbegin - 1]))
      {
	sswap = i;
	f = true;
      }

  char tmp;
  tmp = s[sswap];
  s[sswap] = s[sbegin - 1];
  s[sbegin - 1] = tmp;

  qsort (&s[sbegin], len - sbegin, sizeof (s[0]), ccmp);
  return true;
}

bool
prev_permutation (char s[], const size_t len)
{
  /* find longest subsequence from end that it sorted */
  size_t sbegin = len - 1;
  for (size_t i = 0; i < len - 1 && s[len - i - 2] <= s[len - i - 1]; ++i)
    sbegin = len - i - 2;

  /* whole word is sorted, we can just reverse sort it and return */
  if (sbegin == 0)
    {
      qsort (s, len, sizeof (s[0]), rccmp);
      return false;
    }

  /* find greatest letter less than s[sbegin - 1] to swap with */
  size_t sswap;
  bool f = false;
  for (size_t i = sbegin; i < len; ++i)
    if (s[i] < s[sbegin - 1]
	&& (!f || s[sbegin - 1] - s[i] < s[sbegin - 1] - s[sswap]))
      {
	sswap = i;
	f = true;
      }

  char tmp;
  tmp = s[sswap];
  s[sswap] = s[sbegin - 1];
  s[sbegin - 1] = tmp;

  qsort (&s[sbegin], len - sbegin, sizeof (s[0]), rccmp);
  return true;
}

short
minabsdist (const char s[], const size_t len)
{
  if (len <= 1)
    return 0;

  short d = abs (s[1] - s[0]);
  for (size_t i = 1; i < len - 1; ++i)
    if (abs (s[i + 1] - s[i]) < d)
      d = abs (s[i + 1] - s[i]);

  return d;
}

int
ccmp (const void *const a, const void *const b)
{
  const char *const ap = (const char *) a;
  const char *const bp = (const char *) b;

  return (*ap < *bp) ? -1 : (*ap == *bp) ? 0 : 1;
}

int
rccmp (const void *const a, const void *const b)
{
  const char *const ap = (const char *) a;
  const char *const bp = (const char *) b;

  return (*ap < *bp) ? 1 : (*ap == *bp) ? 0 : -1;
}
